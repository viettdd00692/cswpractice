package com.example.cswpractice.controller;

import com.example.cswpractice.entity.Product;
import com.example.cswpractice.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @GetMapping(value = "/product")
    public List<Product> getAllProducts() {
        return (List<Product>) productRepository.findAll();
    }

    @PostMapping(value = "/product")
    public Product addProduct(@RequestBody Product newProduct) {
        return productRepository.save(newProduct);
    }

    @GetMapping(value = "/product/{id}")
    public Product getProductById(@PathVariable Integer id) {
        return productRepository.findById(id).get();
    }

    @PutMapping(value = "/product/{id}")
    public Product sellProduct(@RequestBody Product newProduct, @PathVariable Integer id) {
        return productRepository.findById(id).map(product -> {
            product.setQuantity(newProduct.getQuantity());
            return productRepository.save(product);
        }).orElseGet(() -> {
            newProduct.setId(id);
            return productRepository.save(newProduct);
        });
    }
}
